# -*- coding: utf-8 -*-
"""
Created on Mon May 25 11:56:29 2020

@author: Lenovo
"""
import cv2
import numpy as geek

image_path= r'C:/Users/redac/Documents/GitLabs/Test2/data/tuSimple/train_set/clips/0313-2/65/18.jpg'
img = cv2.imread(image_path)

    #convertion d'image en gris
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# lissage sans supression des bords
gray_filtered = cv2.bilateralFilter(gray, 7, 150, 150)

    # filtre canny
edges = cv2.Canny(gray, 700, 120)
edges_filtered = cv2.Canny(gray_filtered, 60, 120)

    # impression des 2 images 

images = geek.hstack((edges_filtered, edges))

    # Display the resulting frame
cv2.imshow('Frame', images)

    # Press Q on keyboard to  exit
cv2.waitKey(0)
  

cv2.destroyAllWindows()