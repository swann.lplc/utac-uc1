"""

    Code adapté depuis ros_lane_detect.py
    Le code original necessitait une arrivée d'images via ROS pour fonctionner

"""

import cv2
import numpy as np
import matplotlib.pyplot as plt

import torch
from enet import ENet

from scipy import ndimage as ndi
from sklearn.cluster import DBSCAN

NO_LINE_COLOR = False
LINE_TRANSPARENCY = 1
RESIZE_TO_ORIGINAL = True

# Utilisé pour connaitre le fichier à "tester"
fileName = "C:/Users/redac/Documents/GitLabs/Test2/data/tuSimple/train_set/clips/0313-2/65/18.jpg"

def coloring(mask, gray=False):
    # refer from : https://github.com/nyoki-mtl/pytorch-discriminative-loss/blob/master/src/utils.py
    ins_color_img = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.uint8)
    n_ins = len(np.unique(mask)) - 1
    colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, n_ins)]
    for i in range(n_ins):
        if gray:
            ins_color_img[mask == i + 1] = np.array([255,255,255]).astype(np.uint8)
        else:
            ins_color_img[mask == i + 1] = (np.array(colors[i][:3]) * 255).astype(np.uint8)
            
    return ins_color_img


def gen_instance_mask(sem_pred, ins_pred, n_obj):
    embeddings = ins_pred[:, sem_pred].transpose(1, 0)
    clustering = DBSCAN(eps=0.05).fit(embeddings)
    labels = clustering.labels_

    instance_mask = np.zeros_like(sem_pred, dtype=np.uint8)
    for i in range(n_obj):
        lbl = np.zeros_like(labels, dtype=np.uint8)
        lbl[labels == i] = i + 1
        instance_mask[sem_pred] += lbl

    return instance_mask


class LaneDetectNode(object):
    def __init__(self, model, NO_LINE_COLOR):
        self.model = model
        self.img = None
        self.sem = None
        self.ins = None
        self.out = None
        self.old = None
        self.NO_LINE_COLOR = NO_LINE_COLOR


    def preprocess(self):
        self.img = cv2.resize(self.img, (224,224), interpolation=cv2.INTER_CUBIC)
        self.img = np.uint8(self.img)
        self.old = self.img

        # CLAHE nomalization
        
        self.img = cv2.cvtColor(self.img, cv2.COLOR_RGB2LAB)
        img_plane = cv2.split(self.img)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        img_plane[0] = clahe.apply(img_plane[0])
        self.img = cv2.merge(img_plane)
        self.img = cv2.cvtColor(self.img, cv2.COLOR_LAB2RGB)
        
        self.img = np.array(np.transpose(self.img, (2, 0, 1)), dtype=np.float32)

    def sub_lane(self, img):
        self.img = img
        self.preprocess()

        img_tensor = torch.from_numpy(self.img).unsqueeze(dim=0).cuda()
        sem_pred, ins_pred = self.model(img_tensor)
        sem_pred = sem_pred[:,1,:,:].squeeze(dim=0).cpu().data.numpy()
        sem_pred = ndi.morphology.binary_fill_holes(sem_pred > 0.5)
        ins_pred = ins_pred.squeeze(dim=0).cpu().data.numpy()
        
        self.out = coloring(gen_instance_mask(sem_pred, ins_pred, 8), self.NO_LINE_COLOR)
    

if __name__ == "__main__":

    # Emplacement du fichier de modèle
    model_path = "C:/Users/redac/Documents/GitLabs/Test2/model_best.pth"

    # Paramétrage du modèle et récupération du modèle entrainé
    model = ENet(input_ch=3, output_ch=2).cuda()
    model.load_state_dict(torch.load(model_path))
    model.eval() # Mode d'évaluation
    
    # Récupération de l'image
    img = cv2.imread(fileName)
    
    # Traitement image
    node = LaneDetectNode(model, NO_LINE_COLOR) # Initialisation de l'objet
    node.sub_lane(img) # Envoi et traitement de l'image
    
    # Récupération des deux versions de l'image
    inImg = node.old # Image initile resizée
    outImg = node.out # Image de sortie avec les lignes
    
    x,y,_ = img.shape
    
    if RESIZE_TO_ORIGINAL:
        outImg = cv2.resize(outImg, (y, x), interpolation=cv2.INTER_CUBIC)
        inImg = img
    
    suppImg = cv2.addWeighted(inImg, 1, outImg, LINE_TRANSPARENCY, 0) # Superposition des deux images
    
    # Affichage
    plt.imshow(suppImg)
    plt.show()
