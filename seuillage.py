# -*- coding: utf-8 -*-
"""
Created on Mon May 25 11:56:10 2020

@author: Lenovo
"""


import cv2
image_path= r'C:/Users/redac/Documents/GitLabs/Test2/data/tuSimple/train_set/clips/0313-2/65/18.jpg'
img = cv2.imread(image_path)

    #affichage
cv2.imshow('I am an image display window',img)
cv2.waitKey(0)

    #convertion de l'image en gris
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


#application du seuil pour obtenir une image binaire

threshold=200 #value above which pixel values will be set to max_value
max_value=2500  #value to which pixels above threshold will be set
threshold_stype=cv2.THRESH_BINARY #default threshold method

ret, img_binary = cv2.threshold(img_gray, threshold, max_value, threshold_stype)

#affichage de l'image après le seuillage
cv2.imshow('image after applying threshold',img_binary)
cv2.waitKey(0)

#sauvegarder l'image binaire
#cv2.imwrite('d:/binary.png',img_binary)
cv2.destroyAllWindows()