# -*- coding: utf-8 -*-
"""

Created on Wed May 27 16:24:57 2020

@author: redace

"""

import time
import cv2
import numpy as np
import matplotlib.pyplot as plt

import torch
from enet import ENet

from scipy import ndimage as ndi
from sklearn.cluster import DBSCAN

from lane_detect_from_IMG import *

NO_LINE_COLOR = False
LINE_TRANSPARENCY = 1
RESIZE_TO_ORIGINAL = True
# Utilisé pour connaitre le fichier à "tester"
fileName = "C:\\Users\\redac\\Downloads\\autoroute.mp4"

goToTime = (0, 0) # (minutes, secondes)
FPS = 30


if __name__ == "__main__":

    # Emplacement du fichier de modèle
    model_path = "C:/Users/redac/Documents/GitLabs/Test2/model_best_enet.pth"

    # Paramétrage du modèle et récupération du modèle entrainé
    model = ENet(input_ch=3, output_ch=2).cuda()
    model.load_state_dict(torch.load(model_path))
    model.eval() # Mode d'évaluation
    
    # Récupération de la vidéo
    vid = cv2.VideoCapture(fileName)
    
    # Initialisation du traitement d'image
    node = LaneDetectNode(model, NO_LINE_COLOR) # Initialisation de l'objet
    
    
    if (vid.isOpened()== False): 
        print("Error opening video stream or file")
    
    startFrame = int((goToTime[0] * 60 + goToTime[1]) * FPS)
    print(f"Starting frame is {startFrame}")
    
    vid.set(1, startFrame)
    # Read the video
    while(vid.isOpened()):
        # Capture frame-by-frame
        ret, img = vid.read()
        
        x,y,_ = img.shape
        if ret == True:
            try:
                node.sub_lane(img) # Envoi et traitement de l'image
            except:
                continue
    
            # Récupération des deux versions de l'image
            inImg = node.old # Image initile resizée
            outImg = node.out # Image de sortie avec les lignes
            
            
            if RESIZE_TO_ORIGINAL:
                outImg = cv2.resize(outImg, (y, x), interpolation=cv2.INTER_CUBIC)
                inImg = img
    
            suppImg = cv2.addWeighted(inImg, 1, outImg, LINE_TRANSPARENCY, 0) # Superposition des deux images
            
            msg = f'Traitement neurone'
            cv2.imshow(msg, suppImg)
            
            # Press Q on keyboard to  exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        # Break the loop
        else: 
            break
     
    # When everything done, release the video capture object
    vid.release()
     
    # Closes all the frames
    cv2.destroyAllWindows()
