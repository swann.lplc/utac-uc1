# -*- coding: utf-8 -*-
"""
Created on Mon May 25 15:48:20 2020

@author: Lenovo
"""


import cv2

 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('C:\\Users\\Lenovo\\Documents\\Documents S8\\autoroute.mp4')
 
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read the video
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
      
          # Converting the image to grayscale.
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      
      #apply threshold to gray image to obtain binary image

    threshold=190 #value above which pixel values will be set to max_value
    max_value=1500  #value to which pixels above threshold will be set
    threshold_stype=cv2.THRESH_BINARY #default threshold method

    ret, img_binary = cv2.threshold(gray, threshold, max_value, threshold_stype)

    #display image after thresholding
    cv2.imshow('image after applying threshold',img_binary)

      
      
          # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()